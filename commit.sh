#!/bin/bash
#######################################################################
#
#    Script to update BITBUCKET & PYPI:
#    1) Increment build number
#    2) Commit & Push to BitBucket repository (git)
#    3) Build distributable packed then upload to PYPI (pip)
#######################################################################


if [ "$1A" == "A" ] ; then
    COMMIT_MSG="Not critical"
else
    COMMIT_MSG="$1"
fi

verman.py -b -f version.py
git commit -a -m "$COMMIT_MSG"
git push
python setup.py bdist_wheel upload
