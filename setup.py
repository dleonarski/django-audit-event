#from distutils.core import setup
from setuptools import setup

from version import __version__

setup(
        name='django-audit-event',
        packages=['django_audit_event'],  # this must be the same as the name above
        include_package_data=True,
        version=".".join([str(item) for item in __version__]),
        description='Model of event for audit purposes',
        author='Dariusz Leonarski',
        author_email='dariusz@leonarski.pl',
        url='https://bitbucket.org/dleonarski/django-audit-event.git',
        license='MIT',
        keywords=['django', 'audit', 'event'],  # arbitrary keywords
        classifiers=[
                     'Development Status :: 4 - Beta',
                    'Intended Audience :: Developers',
                    'Topic :: Software Development :: Build Tools',
                    'License :: OSI Approved :: MIT License',
                    'Programming Language :: Python :: 3.4',
            ],
)
